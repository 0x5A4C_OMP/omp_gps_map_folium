import logging
import folium

logging.basicConfig(
     filename='log_file_name.log',
     level=logging.INFO, 
     format= '[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s',
     datefmt='%H:%M:%S'
 )

# set up logging to console
console = logging.StreamHandler()
console.setLevel(logging.DEBUG)
# set a format which is simpler for console use
formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
console.setFormatter(formatter)
# add the handler to the root logger
logging.getLogger('').addHandler(console)

logger = logging.getLogger(__name__)






if __name__ == "__main__":
    logger.info("Starting program")

    import json
    import folium
    from gpxplotter import create_folium_map, read_gpx_file, add_segment_to_map
    import numpy as np
    import pandas as pd
    import altair

    line_options = {'weight': 8}

    the_map = create_folium_map(tiles='openstreetmap')
    for track in read_gpx_file('all.gpx'):
        for i, segment in enumerate(track['segments']):
            add_segment_to_map(the_map, segment, 
                            cmap='viridis', line_options=line_options)


    idx = np.argmax(segment['elevation'])

    WIDTH = 400
    HEIGHT = 200


    def smooth(signal, points):
        """Smooth the given signal using a rectangular window."""
        window = np.ones(points) / points
        return np.convolve(signal, window, mode='same')


    # data = pd.DataFrame(
    #     {
    #         'dist': segment['Distance / km'],
    #         'elevation': segment['elevation'],
    #         'heart': smooth(segment['hr'], 51),
    #     }
    # )

    data = pd.DataFrame(
        {
            'dist': segment['Distance / km'],
            'elevation': segment['elevation']
        }
    )

    altair.data_transformers.enable("vegafusion")

    area1 = altair.Chart(data).mark_area(
        fillOpacity=0.4, strokeWidth=5, line=True
    ).encode(
        x=altair.X('dist', title='Distance / km'),
        y=altair.Y('elevation', title='Elevation / m'),
    )

    line1 = altair.Chart(data).mark_line(
        strokeWidth=5
    ).encode(
        x=altair.X('dist', title='Distance / km'),
        # y=altair.Y('heart', title='Heart rate / bpm'),
        color=altair.value('#1b9e77'),
    )
    chart = altair.layer(
        area1,
        line1,
        width=WIDTH,
        height=HEIGHT,
    ).resolve_scale(y='independent')

    chart.title = 'Elevation & heart rate profile (altair)'

    chart_dict = json.loads(chart.to_json(format="vega"))
    popup = folium.Popup(max_width=WIDTH+100, show=True)
    chart_vega = folium.features.VegaLite(
        chart_dict,
        width=WIDTH+100,
        height=HEIGHT+50
    )
    chart_vega.add_to(popup)
    marker = folium.Marker(
        location=segment['latlon'][idx],
        popup=popup,
        icon=folium.Icon(icon='star'),
    )
    marker.add_to(the_map)

    # To store the map as a HTML page:
    the_map.save('index.html')

    logger.info("Ending program")
